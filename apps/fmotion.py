import xmltodict
from appdaemon.appapi import AppDaemon
import requests
from datetime import datetime


class Fmotion(AppDaemon):
    def initialize(self):
        self.log("Hello from FoscamMotion")
        self.run_every(self.check_motion_callback, datetime.now(), self.args.get('scan_interval',5))

    def check_motion_callback(self, kwargs):
        # self.log('checkmotion')
        res = requests.get("{}/cgi-bin/CGIProxy.fcgi?cmd=getDevState&usr={}&pwd={}".format(
            self.args.get('cam_url'),
            self.args.get('cam_user'),
            self.args.get('cam_passwd'),
        ))
        doc = xmltodict.parse(res.content)
        motion = int(doc['CGI_Result']['motionDetectAlarm'][0])
        sound = int(doc['CGI_Result']['soundAlarm'][0])
        # self.log('{} {}'.format(motion,sound))
        # 0-Disabled, 1-No Alarm, 2-Detect Alarm
        if motion+sound > 2:
            self.set_state("input_boolean.{}".format(self.args.get('device')), state='on')
        else:
            self.set_state("input_boolean.{}".format(self.args.get('device')), state='off')
        # return motion
