from appdaemon.appapi import AppDaemon


class HelloWorld(AppDaemon):
    def initialize(self):
        self.log("Hello from AppDaemon")
        self.log("You are now ready to run Apps!")
